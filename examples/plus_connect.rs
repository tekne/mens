/*
Connect to an EPOC+ device. Currently needs `sudo` to work.
*/

use hidapi::HidApi;
use mens::{ PlusDevice, PlusModel };

fn main() {
    let hidapi = HidApi::new().expect("Could not initialize HID API");
    let epoc = PlusDevice::open(&hidapi);
    match epoc {
        Ok(epoc) => {
            println!("Found EPOC+ device!");
            println!("Manufacturer: {:?}", epoc.device().get_manufacturer_string().unwrap());
            println!("Product: {:?}", epoc.device().get_product_string().unwrap());
            let serial_number = match epoc.device().get_serial_number_string().unwrap() {
                Some(n) => n,
                None => { println!("No serial number detected!"); return; }
            };
            println!("Serial number: {:?}", serial_number);
            println!(
                "Premium key: {:?}",
                PlusModel::Premium.key_from_serial(serial_number.as_bytes())
            );
            println!(
                "Consumer key: {:?}",
                PlusModel::Consumer.key_from_serial(serial_number.as_bytes())
            );
            println!(
                "Standard key: {:?}",
                PlusModel::Standard.key_from_serial(serial_number.as_bytes())
            );
        },
        Err(e) => {
            println!("Error detecting device: {:?}", e);
            println!(
                "HID devices detected:\n-------------------\n{:#?}\n-------------------\n",
                hidapi.devices()
            );
        }
    }
}
