/*!
`mens` is an open-source library for directly connecting to EEG devices from Rust.

Decryption algorithms and research were taken from https://github.com/openyou/emokit-c and
https://github.com/CymatiCorp/CyKit
*/
use hidapi::{
    HidApi,
    HidDevice,
    HidError
};
use std::default::Default;

/// The HID vendor ID for the EPOC+
pub const PLUS_VENDOR_ID: u16 = 0x1234; // = 4660
/// The HID product ID for the EPOC+
pub const PLUS_PRODUCT_ID: u16 = 0xED02; // = 60674

/// The serial number to key array for the premium EPOC+
pub const PREM_KEY: [u8; 16] = [2, 1, 2, 1, 3, 4, 3, 4, 4, 3, 4, 3, 1, 2, 1, 2];
/// The serial number to key array for the consumer EPOC+
pub const CONS_KEY: [u8; 16] = [1, 2, 2, 3, 3, 3, 2, 4, 1, 4, 2, 2, 4, 4, 2, 1];
/// The serial number to key array for the standard (consumer 14-bit) EPOC+
pub const STAN_KEY_TRANS: [u8; 8] = [1, 2, 3, 4, 3, 2, 1, 2];
/// The constants in the standard EPOC+ key
pub const STAN_KEY_CONST: [u8; 8] = [00, 21, 00, 12, 00, 68, 00, 88];

/// EPOC+ models
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum PlusModel {
    /// Premium EPOC+ model
    Premium,
    /// Consumer EPOC+ model, 16-bit EPOC+ mode
    Consumer,
    /// Consumer EPOC+ model, 14-bit EPOC mode
    Standard
}

/// Get a character from the back of a serial number, or error
fn back(serial_number: &[u8], pos: usize) -> Result<u8, KeyError> {
    if serial_number.len() > pos && pos > 0 {
        Ok(serial_number[serial_number.len() - pos])
    } else {
        Err(KeyError::TooShort)
    }
}

/// AES256 key for EPOC+ decryption
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct PlusKey(pub [u8; 16]);

impl PlusModel {

    /// Get the AES256 key given a serial number for the EPOC+ Premium model
    pub fn premium_key_from_serial(serial_number: &[u8]) -> Result<PlusKey, KeyError> {
        // Taken from CyKit, which is in the public domain
        let s = serial_number;
        let mut k: [u8; 16] = Default::default();
        for (i, sh) in PREM_KEY.iter().enumerate() {
            let sh = *sh as usize;
            if s.len() < sh {
                return Err(KeyError::TooShort)
            }
            k[i] = s[s.len() - sh];
        }
        Ok(PlusKey(k))
    }
    /// Get the AES256 key given a serial number for the EPOC+ Consumer model, 16-bit mode
    pub fn consumer_key_from_serial(serial_number: &[u8]) -> Result<PlusKey, KeyError> {
        // Taken from CyKit, which is in the public domain
        let s = serial_number;
        let mut k: [u8; 16] = Default::default();
        for (i, sh) in CONS_KEY.iter().enumerate() {
            let sh = *sh as usize;
            if s.len() < sh {
                return Err(KeyError::TooShort)
            }
            k[i] = s[s.len() - sh];
        }
        Ok(PlusKey(k))
    }
    /// Get the AES256 key given a serial number for the EPOC+ Consumer model, 14-bit mode
    pub fn standard_key_from_serial(serial_number: &[u8]) -> Result<PlusKey, KeyError> {
        // Taken from CyKit, which is in the public domain
        let s = serial_number;
        let mut k: [u8; 16] = Default::default();
        for i in 0..8 {
            let sh = STAN_KEY_TRANS[i] as usize;
            if s.len() < sh {
                return Err(KeyError::TooShort)
            }
            k[2*i] = s[s.len() - sh];
            k[2*i + 1] = STAN_KEY_CONST[i]
        }
        Ok(PlusKey(k))
    }
    /// Get the AES256 key given a serial number for the given EPOC+ model
    pub fn key_from_serial(&self, serial_number: &[u8]) -> Result<PlusKey, KeyError> {
        use PlusModel::*;
        match self {
            Premium => Self::premium_key_from_serial(serial_number),
            Consumer => Self::consumer_key_from_serial(serial_number),
            Standard => Self::standard_key_from_serial(serial_number)
        }
    }
}

/// A handle for the EPOC+
pub struct PlusDevice(HidDevice);

impl PlusDevice {
    /// Get the underlying HID device
    pub fn device(&self) -> &HidDevice { &self.0 }
    /// Try to connect to an EPOC+ device given a HID device handle
    pub fn new(device: HidDevice) -> Result<PlusDevice, EEGConnectionError> {
        //TODO: logic, checks, serial number, etc.
        Ok(PlusDevice(device))
    }
    /// Try to open an EPOC+ device using `hidapi`
    pub fn open(api: &HidApi) -> Result<PlusDevice, EEGConnectionError> {
        let device = api
            .open(PLUS_VENDOR_ID, PLUS_PRODUCT_ID)
            .map_err(|e| EEGConnectionError::HidError(e))?;
        Self::new(device)
    }
}

/// An error connecting to an EEG device
#[derive(Debug)]
pub enum EEGConnectionError {
    /// No EEG device was found connected to this computer
    NoDeviceFound,
    /// A HID error
    HidError(HidError),
    /// An error generating an AES key
    KeyError(KeyError)
}

/// An error generating an AES key
#[derive(Debug)]
pub enum KeyError {
    /// The given serial number was too short
    TooShort
}
